import React from 'react'
import Swiperslides from '../Components/Swiperslides'
import Card1 from '../Cards/Card1'

const Home = () => {
  return (
    <div>
      <Card1 />
     <Swiperslides/>
    </div>
  )
}

export default Home
