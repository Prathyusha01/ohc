import React, { useState } from 'react';

const Login = () => {
  const [Email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [Register, setRegister] = useState('');

  const handleLogin = () => {
    
    if (Email === 'admin' && password === 'password') {
    
      console.log('Login successful');
      
    
    } else if (Register === 'form' && Register === 'Register'){

        console.log('To Register')
    }
    else {
    
      setError('Invalid Email or password');
    }

  };

  return (
    <div class ="container">
      <h2>Login</h2>
      {error && <p style={{ color: 'red' }}>{error}</p>}
      <div>
        <input type="text" Placeholder= "Enter Your Email" value={Email} onChange={(e) => setEmail(e.target.value)} />
      </div>
      <div>
        <input type="password" Placeholder="Enter Your Password" value={password} onChange={(e) => setPassword(e.target.value) } />
      </div>
      <div>
        <p>Don't have an account?<a href='http://localhost:3000/SignUp' className='register'>register</a></p>
      </div>
      <button onClick={handleLogin}>login</button>
    </div>
  );
};

export default Login;