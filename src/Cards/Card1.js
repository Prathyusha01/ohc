import React from "react";
import {NavLink} from 'react-router-dom';
import {Card} from 'reactstrap';
import {CardBody} from 'reactstrap';
import{CardTitle} from 'reactstrap';
import {CardText} from 'reactstrap';
import i2 from '../Assets/i2.jpg';
import img1 from '../Assets/img1.jpg';
import img2 from '../Assets/img2.jpg';
import img3 from '../Assets/img3.jpg'


function CompCard(){
  return(
<div>
  <div class="card col my-2 col-sm-6 com-md-2 col-lg-1" >
  <NavLink><Card style={{ width: '8rem'}}>
         <img src={img1} alt="logo" className='i' />
         <CardBody>
            <CardTitle tag="h5">Doctor Consultation</CardTitle>
            <CardText></CardText>
         </CardBody>
     </Card></NavLink>
  </div>
  <div class="card col my-2 col-sm-6 com-md-2 col-lg-1" >
        <NavLink><Card style={{ width: '8rem'}}>
          <img src={img3} alt="logo" className='i' />
            <CardBody>
            <CardTitle tag="h5">Health Checks & Tests</CardTitle>
            <CardText></CardText>
            </CardBody>
        </Card></NavLink>
  </div>
  <div class="card col my-2 col-sm-6 com-md-2 col-lg-1" >
  <NavLink><Card style={{ width: '8rem'}}>
          <img src={img2} alt="logo" className='i' />
            <CardBody>
            <CardTitle tag="h5">Order Medicines</CardTitle>
            <CardText></CardText>
            </CardBody>
        </Card></NavLink>
  </div>
  <div class="card col my-2 col-sm-6 com-md-2 col-lg-1" >
  <NavLink><Card style={{ width: '8rem'}}>
          <img src={i2} alt="logo" className='i' />
            <CardBody>
            <CardTitle tag="h5">Nearby Labs</CardTitle>
            <CardText></CardText>
            </CardBody>
        </Card></NavLink>
  </div>
  

</div>
)}
export default CompCard;