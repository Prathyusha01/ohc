import React, { useState } from "react";
import axios from 'axios'
import { useNavigate } from "react-router-dom";
const url = 'localhost:2000/user/login'

const UserLogin = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('')
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post(url, {
        email: email,
        password: parseInt(password)
      });

      if (response.status === 200) {
        console.log("Login Success");
        clearFileds();
        return navigate('/')
      }
    } catch (err) {
      console.log(err);
      clearFileds()
    }
  }
  const clearFileds = () => {
    setEmail('')
    setPassword('')
  }
  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <fieldset>
          <div>
            {/* <label htmlFor="email">Username or Email</label> */}
            <input
              type="email"
              id="email"
              name="email"
              placeholder="Username or Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>

          <div>
            {/* <label htmlFor="password">password</label> */}
            <input
              type="password"
              id="password"
              name="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <p>Didn't have an account?<a href="http://localhost:3000/SignUp">SignUp</a></p>
          </div>

          <div>
            <button type="submit">Login</button>
          </div>
        </fieldset>
      </form>
    </div>
  );
};

export default UserLogin;