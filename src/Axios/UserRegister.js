import axios from "axios";
import React, { useState } from "react";
const url = "localhost:2000/user/signup"

const UserRegister = () => {
    const[id,setId]=useState("");
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [gender, setGender] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      const data = {
        id,
        name,
        age,
        gender,
        email,
        password
      }
      const response = axios.post(url, data);
      console.log(response.data);
      clearFields();

    } catch (err) {
      console.log(err);
    }
  }
  const clearFields = () => {
    setId('')
    setName('')
    setAge('')
    setGender('')
    setEmail('')
    setPassword('')
  }
  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <fieldset>
        <div>
            <label htmlFor="id">Id</label>
            <input
              type="text"
              id="id"
              name="id"
              value={id}
              onChange={(event) => {
                setId(event.target.value);
              }}
            />
          </div>
          <div>
            <label htmlFor="name">Name</label>
            <input
              type="text"
              id="name"
              name="name"
              value={name}
              onChange={(event) => {
                setName(event.target.value);
              }}
            />
          </div>
          <div>
            <label htmlFor="age">Age</label>
            <input
              type="text"
              id="age"
              name="age"
              value={age}
              onChange={(event) => {
                setAge(event.target.value);
              }}
            />
          </div>

          <div>
            <label htmlFor="gender">Gender</label>
            <input
              type="text"
              id="gender"
              name="gender"
              value={gender}
              onChange={(event) => {
                setGender(event.target.value);
              }}
            />
          </div>

          <div>
            <label htmlFor="email">Email</label>
            <input
              type="text"
              id="email"
              name="email"
              value={email}
              onChange={(event) => {
                setEmail(event.target.value);
              }}
            />
          </div>

          <div>
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              name="password"
              value={password}
              onChange={(event) => {
                setPassword(event.target.value);
              }}
            />
          </div>
          <div>
            <p>Already have an account?<a href="http://localhost:3000/Login">Login</a></p>
          </div>
          <div>
            <button type="submit">signup</button>
          </div>
        </fieldset>
      </form>
    </div>
  );
};

export default UserRegister;