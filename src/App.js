import {Routes,Route} from 'react-router-dom'
import './App.css';
import Header from './Components/Header'
import Footer from './Components/Footer'
import Home from './Pages/Home';
import Services from './Pages/Services';
import UserLogin from './Axios/UserLogin';
import UserRegister from './Axios/UserRegister';

function App() {
  return (
    <div className="App">
     <Header />
     <Routes>
      <Route path='/' element={<Home/>}/>
      {/* <Route path='/About' element={<About/>}/> */}
      <Route path='/Services' element={<Services/>}/>
      <Route path='/Login' element={<UserLogin/>}/>
      <Route path='/SignUp' element={<UserRegister/>}/>

     </Routes>
     <Footer />
    </div>
  );
}

export default App;
