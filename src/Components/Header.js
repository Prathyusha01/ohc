import React from 'react'
import {NavLink} from 'react-router-dom'
import logo from '../Assets/logo.jpg'
import { RiMenuSearchLine } from "react-icons/ri";


const Header = () => {
  return (
    <div>
        <header>
            <NavLink to='/' className='link'>
            <input type='checkbox' id='check'/>
            <label htmlFor='check' className='btn-toggle'><span><RiMenuSearchLine /></span></label>
            <img src={logo} alt="logo" className='icon' />
            </NavLink>
            <nav className='navigation'>
              <ul>
                <li><input type='text' placeholder='search' id='search'/></li>
                <li> <NavLink to='/Services' className='link'> Our Services</NavLink></li>
                <li> <NavLink to='/Login' className='link'>About Us</NavLink></li>
                <li> <NavLink to='/Login' className='link'>Login</NavLink></li>
              </ul>
            </nav>
        </header>
    </div>
    
  )
}

export default Header