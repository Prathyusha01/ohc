import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/swiper-bundle.css'
import { Autoplay, EffectCards, EffectCube, Navigation, Pagination, Scrollbar } from 'swiper/modules'
import i3 from '../Assets/i3.jpg'
import ii from '../Assets/ii.png'
import ii2 from '../Assets/ii2.webp'
import ii3 from '../Assets/ii3.jpg'
import ii5 from '../Assets/ii5.webp'


const Swiperslides = () => {
    return (

        <div className='swiperComponant' >
            <Swiper
                modules={[Navigation, Pagination, Scrollbar,Autoplay,EffectCards]}
                spaceBetween={50}
                // navigation
                // pagination={{ clickable: true }}
                // scrollbar={{ draggable: true }}
                slidesPerView={1}
                autoplay={{ delay: 3000 }}
                effect='card'
                loop={true}
                speed={3000}
            >
          <SwiperSlide>
                    <img src={i3} className='Card-img-top' alt='...' />
          </SwiperSlide>
          <SwiperSlide>
                    <img src={ii2} className='Card-img-top' alt='...' />
          </SwiperSlide>
          <SwiperSlide>
                    <img src={ii5} className='Card-img-top' alt='...' />
          </SwiperSlide>
          <SwiperSlide>
                    <img src={ii} className='Card-img-top' alt='...' />
          </SwiperSlide>
          <SwiperSlide>
                    <img src={ii3} className='Card-img-top' alt='...' />
          </SwiperSlide>
                


            </Swiper>
        </div>

    )
}

export default Swiperslides